package face.entity;

import org.bytedeco.javacpp.opencv_core.Mat;

public class Face{
	private String id;
	private Mat faceMat;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Mat getFaceMat() {
		return faceMat;
	}
	public void setFaceMat(Mat faceMat) {
		this.faceMat = faceMat;
	}
	
}