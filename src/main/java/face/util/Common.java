package face.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
/**
 * 公共变量 (后期放入配置文件，这样不用编译java类)
 * @author ShiQiang
 */
public class Common {
	
	public static final Config ROOTCONFIG =  ConfigFactory.load("conf/config.conf").getConfig("config");
	
	/*脸部信息保存*/
	public static String saveFacePath = ROOTCONFIG.getString("saveFacePath");
	/**/
	public static String saveOriFacePath = ROOTCONFIG.getString("saveOriFacePath");
	//面部识别模型
	public static String faceDetectPath = ROOTCONFIG.getString("faceDetec");
	//眼睛识别模型
	public static String eyesCascadePath = ROOTCONFIG.getString("eyesCascade"); 
	
	//人脸关键点识别
	public static String facePointDetectPath = ROOTCONFIG.getString("facePointDetect");
	 
	public static String facePointModelPath = ROOTCONFIG.getString("facePointModelPath"); 
	
	
	//对检测出的头像进行放缩处理
	public static int faceWidth = ROOTCONFIG.getInt("faceWidth");
	public static int faceHeight = ROOTCONFIG.getInt("faceHeight");
	
	//人脸训练的模型
	public static String trainModelPath = ROOTCONFIG.getString("trainModelPath"); 
	
	//视频截取间隔时间毫秒
	public static long cutInterval = ROOTCONFIG.getInt("cutInterval");
	
	//无效的占位人脸图片
	public static String invalidFace = ROOTCONFIG.getString("invalidFacePath");
	
	
		
}
